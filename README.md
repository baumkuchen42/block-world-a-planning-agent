# Block World

An example application of how intelligent agents can plan to solve problems.

This agent rearranges a block tower on the left side to fit the target specification on the right side.

![image](/uploads/a597b91573925b81f6b43e7d0cfbbefd/image.png)

![image](/uploads/ae1a3965dd074938ba20af093ac44d15/image.png)
