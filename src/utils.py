from gi.repository import Gtk, Gdk

def notify(window, text, further_explanation=''):
	msg = Gtk.MessageDialog(
		transient_for=window,
		message_type=Gtk.MessageType.INFO,
		buttons=Gtk.ButtonsType.OK,
		text=text
	)
	msg.format_secondary_text(further_explanation)
	msg.run()
	msg.destroy()

def load_css():
	css_provider = Gtk.CssProvider()
	css_provider.load_from_resource('/de/hszg/blockworld/style.css')
	screen = Gdk.Screen.get_default()
	style_context = Gtk.StyleContext()
	style_context.add_provider_for_screen(screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_USER)
