import time, random

from threading import Thread, Event

from gi.repository import GLib, Gdk

class Planner(Thread):
	MOVING_DELAY = .5
	exit_event = Event()

	def __init__(self, gui):
		super().__init__()
		self.gui = gui

		self.goal_slot = next(iter(self.gui.goal_tower))[0]
		self.start_slot = next(iter(self.gui.solving_tower))[0]
		self.solving_slots = [
			slot for slot in range(self.gui.slots_max_index+1)
			if not (slot == self.goal_slot or slot == self.start_slot)
		]

	def run(self):
		time.sleep(.2)
		i = 0
		while True:
			if self.exit_event.is_set():
				return
			if self.gui.solving_tower != self.gui.goal_tower:
				next_goal_color = self.gui.goal_tower[(self.goal_slot, self.gui.block_tower_max_index - i)]
				next_block_position = self.find_block(next_goal_color, self.gui.solving_tower)
				self.clear_over(next_block_position)
				GLib.idle_add(self.gui.move_on_tower, next_block_position, self.goal_slot)
				i += 1
				time.sleep(self.MOVING_DELAY)
			else:
				GLib.idle_add(self.gui.toggle_solving) # on successful solving
				return

	def find_block(self, color: Gdk.Color, tower: dict) -> tuple:
		for key, value in tower.items():
			if value == color:
				return key

	def clear_over(self, block_position: tuple):
		neighbour_blocks = [(x, y) for x, y in self.gui.solving_tower.keys() if x == block_position[0]]
		neighbour_blocks.sort()
		for x, y in neighbour_blocks:
			if self.exit_event.is_set():
				return
			if y < block_position[1]:
				self.move_to_intermediate_slot((x, y))
				time.sleep(self.MOVING_DELAY)

	def move_to_intermediate_slot(self, position: tuple):
		if self.exit_event.is_set():
			return
		if position[0] in self.solving_slots:
			without_pos_slots = [s for s in self.solving_slots if s != position[0]]
			slot = self.start_slot if not without_pos_slots else random.choice(without_pos_slots)
		else:
			slot = self.solving_slots[0]
		GLib.idle_add(self.gui.move_on_tower, position, slot)
