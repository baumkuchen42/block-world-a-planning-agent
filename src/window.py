# window.py
#
# Copyright 2021 Uta
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import signal

from gi.repository import Gtk, Gdk

from .planner import Planner

from .utils import load_css, notify


@Gtk.Template(resource_path='/de/hszg/blockworld/window.ui')
class BlockworldTask4Window(Gtk.ApplicationWindow):
	__gtype_name__ = 'BlockworldTask4Window'

	block_color_picker = Gtk.Template.Child()
	solving_grid = Gtk.Template.Child()
	goal_grid = Gtk.Template.Child()
	remove_slot_btn = Gtk.Template.Child()
	add_slot_btn = Gtk.Template.Child()
	decr_block_tower_height_btn = Gtk.Template.Child()
	incr_block_tower_height_btn = Gtk.Template.Child()
	start_stop_btn = Gtk.Template.Child()
	start_stop_img = Gtk.Template.Child()

	slots_max_index = 2
	block_tower_max_index = 3

	planner_agent = NotImplemented
	solving_tower = {}
	goal_tower = {}

	MOVING_TIME = .5

	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		load_css()
		self.show_welcome_dialog()
		self.connect_events()

	def show_welcome_dialog(self):
		notify(
			self,
			'Welcome to Block Worlds',
			'''
			This is an agent that solves a puzzle:
			It arranges the blocks on the left side so that they match the right side.\n
			You build the towers of blocks on each side that the agent will rearrange.
			You can choose the colors from the colorpicker on the top.\n
			Take care to use the same colors on the left side as on the right.
			'''
		)

	def connect_events(self):
		for btn in self.solving_grid:
			btn.get_style_context().add_class('normal')
			btn.connect('clicked', self.toggle_color_change)
		for btn in self.goal_grid:
			btn.get_style_context().add_class('normal')
			btn.connect('clicked', self.toggle_color_change)

		self.remove_slot_btn.connect('clicked', self.remove_slot)
		self.add_slot_btn.connect('clicked', self.add_slot)
		self.decr_block_tower_height_btn.connect('clicked', self.decr_block_tower_height)
		self.incr_block_tower_height_btn.connect('clicked', self.incr_block_tower_height)

		self.start_stop_btn.connect('clicked', self.toggle_solving)

	def toggle_color_change(self, btn, color=None):
		if not btn.get_style_context().has_class('normal'):
			btn.get_style_context().add_class('normal')
		else:
			btn.get_style_context().remove_class('normal')
			current_color = color or self.block_color_picker.get_rgba()
			btn.override_background_color(Gtk.StateFlags.NORMAL, current_color)

	def remove_slot(self, widget=None):
		if self.slots_max_index > 2:
			self.solving_grid.remove_column(self.slots_max_index)
			self.goal_grid.remove_column(self.slots_max_index)
			self.slots_max_index -= 1
		else:
			notify(self, 'You cannot remove any more slots.')

	def add_slot(self, widget=None):
		self.slots_max_index += 1
		for i in range(self.block_tower_max_index+1):
			btn = self.create_new_block()
			self.solving_grid.attach(btn, self.slots_max_index, i, 1, 1)
		for i in range(self.block_tower_max_index+1):
			btn = self.create_new_block()
			self.goal_grid.attach(btn, self.slots_max_index, i, 1, 1)

	def decr_block_tower_height(self, widget=None):
		self.solving_grid.remove_row(0)
		self.goal_grid.remove_row(0)
		self.block_tower_max_index -= 1

	def incr_block_tower_height(self, widget=None):
		'''
		The attaching process might seem a bit counterinuitive.
		It's because we are treating the row at the top of the window as the top one
		while the grid is treating it as its bottom row.
		'''
		self.block_tower_max_index += 1
		self.solving_grid.insert_next_to(self.solving_grid.get_child_at(0, 0), Gtk.PositionType.TOP)
		self.goal_grid.insert_next_to(self.goal_grid.get_child_at(0, 0), Gtk.PositionType.TOP)
		for i in range(self.slots_max_index+1):
			btn = self.create_new_block()
			self.solving_grid.attach(btn, i, 0, 1, 1)
		for i in range(self.slots_max_index+1):
			btn = self.create_new_block()
			self.goal_grid.attach(btn, i, 0, 1, 1)

	def create_new_block(self):
		btn = Gtk.Button()
		btn.set_relief(Gtk.ReliefStyle.NONE)
		btn.set_visible(True)
		btn.get_style_context().add_class('normal')
		btn.connect('clicked', self.toggle_color_change)
		return btn

	def toggle_solving(self, widget=None):
		if self.is_solving():
			self.planner_agent.exit_event.set()
			self.set_solving(False)

		else:
			self.solving_tower = self.find_tower(self.solving_grid)
			self.goal_tower = self.find_tower(self.goal_grid)

			if self.are_towers_correct(self.solving_tower, self.goal_tower):
				self.planner_agent = Planner(self)
				self.planner_agent.start()
				self.set_solving(True)

			else:
				notify(
					self,
					'Your block towers are not correctly defined.',
					'''
					Check if the towers on both sides are of the same height
					and consist of the same colors.

					Take care that your towers start at ground level too and that
					they are not on the same position.
					'''
				)


	def is_solving(self):
		return self.start_stop_img.get_icon_name()[0] == 'media-playback-stop-symbolic'

	def set_solving(self, solving: bool):
		if solving is True:
			self.start_stop_img.set_from_icon_name('media-playback-stop-symbolic', Gtk.IconSize.BUTTON)
		else:
			self.start_stop_img.set_from_icon_name('media-playback-start-symbolic', Gtk.IconSize.BUTTON)

	def are_towers_correct(self, solving_tower, goal_tower):
		goal_slot = next(iter(self.goal_tower))[0]
		start_slot = next(iter(self.solving_tower))[0]

		return (
			len(solving_tower) == len(goal_tower)
			and self.do_tower_colors_match(solving_tower, goal_tower)
			and goal_slot != start_slot
		)

	def find_tower(self, grid):
		tower = {}
		for i in range(self.slots_max_index+1):
			child = grid.get_child_at(i, self.block_tower_max_index)
			if self.is_block(child):
				tower[(i, self.block_tower_max_index)] = child.get_style_context().get_background_color(Gtk.StateFlags.NORMAL)
				for j in range(self.block_tower_max_index-1, -1, -1):
					child = grid.get_child_at(i, j)
					if self.is_block(child):
						tower[(i, j)] = child.get_style_context().get_background_color(Gtk.StateFlags.NORMAL)
				break
		return tower

	def do_tower_colors_match(self, solving_tower, goal_tower):
		for color in solving_tower.values():
			if color not in goal_tower.values():
				return False
		return True

	def move_on_tower(self, current_position: tuple, tower_column):
		top_index = self.block_tower_max_index+1
		for i in range(self.block_tower_max_index, -1, -1):
			child = self.solving_grid.get_child_at(tower_column, i)
			if self.is_block(child):
				top_index = i
		if top_index != 0:
			self.move_block(current_position, (tower_column, top_index-1))
		else:
			print('Error: Could not move block. Tower has reached maximum height.')

	def is_block(self, btn):
		return not btn.get_style_context().has_class('normal')

	def move_block(self, current_position: tuple, target_position: tuple):
		current = self.solving_grid.get_child_at(current_position[0], current_position[1])
		target = self.solving_grid.get_child_at(target_position[0], target_position[1])
		color = current.get_style_context().get_background_color(Gtk.StateFlags.NORMAL)
		self.toggle_color_change(current)
		self.toggle_color_change(target, color)
		self.solving_tower[target_position] = self.solving_tower.pop(current_position)
